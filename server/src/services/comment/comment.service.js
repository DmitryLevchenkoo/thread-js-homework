class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  updateById(id, body) {
    return this._commentRepository.updateById(id, body);
  }

  deleteById(id) {
    return this._commentRepository.updateById(id, { isActive: false });
  }
}

export { Comment };
