import { DataTypes } from 'sequelize';

const init = orm => {
  const Comment = orm.define(
    'comment',
    {
      body: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      isActive: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return Comment;
};

export { init };
