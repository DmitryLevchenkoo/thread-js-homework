'use strict';

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.transaction(transaction =>
      Promise.all([
        queryInterface.addColumn(
          'posts',
          'isActive',
          {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
          },
          { transaction }
        )
      ])
    ),

  /**
   * Add altering commands here.
   *
   * Example:
   * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
   */

  down: queryInterface =>
    queryInterface.sequelize.transaction(transaction =>
      Promise.all([
        queryInterface.removeColumn('posts', 'isActive', { transaction })
      ])
    )
};
