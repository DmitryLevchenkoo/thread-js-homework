import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_EDITABLE_POST: 'thread/set-editable-post',
  UPDATE_POST: 'thread/update_post',
  DELETE_POST: 'thread/delete_post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));
const deletePostAction = createAction(ActionType.DELETE_POST, post => ({
  payload: {
    post
  }
}));

const updatePostAction = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));
const setEditablePost = createAction(ActionType.SET_EDITABLE_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost =
  ({ post, id }) =>
  async dispatch => {
    const { id: updatedPostId } = await postService.updatePost({
      payload: post,
      id
    });
    const updatedPost = await postService.getPost(updatedPostId);
    dispatch(updatePostAction(updatedPost));
  };

const commentUpdate =
  ({ comment, id }) =>
  async dispatch => {
    await commentService.updateComment({
      body: comment,
      id
    });
  };
const commentDelete =
  ({ id, postId }) =>
  async (dispatch, getRootState) => {
    await commentService.deleteComment(id);
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: post.comments
        ? [...post.comments.filter(el => el.id !== id)]
        : null // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getRootState();

    const updated = posts.map(post =>
      post.id !== postId ? post : mapComments(post)
    );

    dispatch(setPosts(updated));

    dispatch(setExpandedPost(mapComments(expandedPost)));
  };
const deletePost = id => async dispatch => {
  await postService.deletePost(id);
  const posts = await postService.getAllPosts();
  dispatch(setPosts(posts));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};
const toggleEditablePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditablePost(post));
};
const editPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditablePost(post));
};
const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapLikes = post =>
    createdAt === updatedAt
      ? { ...post, likeCount: Number(post.likeCount) + diff }
      : {
          ...post,
          likeCount: Number(post.likeCount) + diff,
          dislikeCount: Number(post.dislikeCount) - diff
        };
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post =>
    post.id !== postId ? post : mapLikes(post)
  );

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapDislikes = post =>
    createdAt === updatedAt
      ? { ...post, dislikeCount: Number(post.dislikeCount) + diff }
      : {
          ...post,
          dislikeCount: Number(post.dislikeCount) + diff,
          likeCount: Number(post.likeCount) - diff
        };

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post =>
    post.id !== postId ? post : mapDislikes(post)
  );

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};
const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post =>
    post.id !== comment.postId ? post : mapComments(post)
  );

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditablePost,
  updatePostAction,
  deletePostAction,
  commentUpdate,
  commentDelete,
  updatePost,
  loadPosts,
  deletePost,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  toggleEditablePost,
  likePost,
  editPost,
  dislikePost,
  addComment
};
