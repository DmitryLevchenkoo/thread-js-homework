import { createReducer } from '@reduxjs/toolkit';
import {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditablePost,
  updatePostAction
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  editablePost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const posts = action.payload.posts.filter(post => post.isActive === true);

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const posts = action.payload.posts.filter(post => post.isActive === true);

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(updatePostAction, (state, action) => {
    const { post } = action.payload;
    state.posts = state.posts.map(el => (el.id === post.id ? post : el));
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(setEditablePost, (state, action) => {
    const { post } = action.payload;

    state.editablePost = post;
  });
});

export { reducer };
