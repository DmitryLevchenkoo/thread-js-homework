import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }
  updateComment({ id, body }) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ body })
    });
  }
  deleteComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.DELETE,
      contentType: ContentType.JSON
    });
  }
  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { Comment };
