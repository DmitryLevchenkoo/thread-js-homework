import React from 'react';
import { Spinner, Modal } from 'src/components/common/common';
import { threadActionCreator } from 'src/store/actions';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment } from 'src/components/common/common';

const EditPost = ({ editablePost, uploadImage, postUpdate, deletePost }) => {
  const dispatch = useDispatch();
  //load exists data
  const initBody = editablePost.body ? editablePost.body : '';
  const initImage =
    editablePost?.image?.id && editablePost?.image?.link
      ? { imageId: editablePost.image.id, imageLink: editablePost.image.link }
      : undefined;

  const [body, setBody] = React.useState(initBody);
  const [image, setImage] = React.useState(initImage);
  const [isUploading, setIsUploading] = React.useState(false);
  const [isUpdating, setIsUpdating] = React.useState(false);
  const [isDeleting, setIsDeleting] = React.useState(false);

  const handleExpandedPost = React.useCallback(
    id => dispatch(threadActionCreator.toggleEditablePost(id)),
    [dispatch]
  );
  const handleExpandedPostClose = () => handleExpandedPost();
  const handleUpdatePost = async () => {
    //empty post
    if (!body) {
      return handleExpandedPost();
    }
    //same data
    if (
      body === initBody &&
      image === editablePost?.image?.id &&
      editablePost?.image?.link
        ? { imageId: editablePost.image.id, imageLink: editablePost.image.link }
        : undefined
    ) {
      return handleExpandedPost();
    }

    try {
      setIsUpdating(true);
      await postUpdate({
        post: { imageId: image?.imageId, body },
        id: editablePost.id
      });
    } catch (e) {
      console.log(e);
    } finally {
      setIsUpdating(false);
    }

    return handleExpandedPost();
  };
  const handleDeletePost = async (e, id) => {
    e.preventDefault();
    //TODO modal wimdow
    try {
      setIsDeleting(true);
      await deletePost(editablePost.id);
    } catch (e) {
      console.log(e);
    } finally {
      setIsDeleting(false);
    }
    return handleExpandedPost();
  };
  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(error => {
        console.log(error);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };
  return (
    <Modal
      dimmer="blurring"
      centered={true}
      open
      onClose={handleExpandedPostClose}
    >
      {editablePost ? (
        <>
          <div className={styles.text_container}>
            <h2>EDIT POST</h2>
          </div>
          <div className={styles.addPostForm}>
            <Segment>
              <Form onSubmit={handleUpdatePost}>
                <Form.TextArea
                  name="body"
                  value={body}
                  placeholder="What is the news?"
                  onChange={ev => setBody(ev.target.value)}
                />
                {image?.imageLink && (
                  <div className={styles.imageWrapper}>
                    <Image
                      className={styles.image}
                      src={image?.imageLink}
                      alt="post"
                    />
                  </div>
                )}
                <div className={styles.btnWrapper}>
                  <div className={styles.relativeButtonWrapper}>
                    <div className={styles.leftbtnWrapper}>
                      <Button
                        color="teal"
                        isLoading={isUploading}
                        isDisabled={isUploading}
                        iconName={IconName.IMAGE}
                        isDisabled={isUpdating || isUploading || isDeleting}
                      >
                        <label className={styles.btnImgLabel}>
                          {image ? 'Change image' : 'Attach image'}
                          <input
                            name="image"
                            type="file"
                            onChange={handleUploadFile}
                            hidden
                          />
                        </label>
                      </Button>
                      <Button
                        color={ButtonColor.BLUE}
                        type={ButtonType.SUBMIT}
                        isDisabled={isUpdating || isUploading || isDeleting}
                      >
                        Update
                      </Button>
                    </div>
                    <div className={styles.rightbtnWrapper}>
                      <Button
                        color={ButtonColor.RED}
                        type={ButtonType.SUBMIT}
                        isDisabled={isUpdating || isUploading}
                        onClick={e => handleDeletePost(e, editablePost.id)}
                        isDisabled={isUpdating || isUploading || isDeleting}
                      >
                        Delete
                      </Button>
                    </div>
                  </div>
                </div>
              </Form>
            </Segment>
          </div>
        </>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};
EditPost.propTypes = {
  editablePost: PropTypes.object.isRequired,
  uploadImage: PropTypes.func.isRequired,
  postUpdate: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};
export default EditPost;
