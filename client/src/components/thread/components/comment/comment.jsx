import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import {
  Comment as CommentUI,
  Icon,
  Label,
  Input
} from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import { IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body: initBody, createdAt, user, id, postId },
  userId
}) => {
  const [body, setBody] = React.useState(initBody);
  const [isEditMode, setEditMode] = React.useState(false);
  const dispatch = useDispatch();

  const handleCommentUpdate = React.useCallback(
    ({ body, id }) =>
      dispatch(threadActionCreator.commentUpdate({ comment: body, id })),
    [dispatch]
  );

  const handleCommentDelete = React.useCallback(
    id => dispatch(threadActionCreator.commentDelete({ id, postId })),
    [dispatch]
  );

  const setEditModeHandler = () => {
    setEditMode(!isEditMode);
    initBody !== body && Boolean(body)
      ? handleCommentUpdate({ id, body })
      : null;
  };
  const renderEditIcon = () =>
    isEditMode ? (
      <Icon className={styles.icon} name={IconName.CHECKMARK} />
    ) : (
      <Icon className={styles.icon} name={IconName.EDIT} />
    );
  const handleChangeInput = ev => setBody(ev.target.value);
  const handleKeyDown = k => (k.key === 'Enter' ? setEditModeHandler() : null);
  const renderCommentBody = () =>
    isEditMode ? (
      <Input
        onChange={handleChangeInput}
        onKeyDown={handleKeyDown}
        value={body}
        className={styles.input}
      />
    ) : (
      <CommentUI.Text>{body}</CommentUI.Text>
    );
  const renderIcon = ({ icoName }) => (
    <Icon className={styles.icon} name={icoName} />
  );

  const renderIconLabel = ({ onPress, handler, icoName, params }) =>
    user.id === userId ? (
      <Label
        basic
        size="small"
        as="a"
        className={styles.label}
        onClick={() => onPress(params)}
      >
        {handler ? handler() : renderIcon({ icoName })}
      </Label>
    ) : null;

  const labels = [
    {
      name: 'edit',
      icoName: IconName.EDIT,
      handler: renderEditIcon,
      pressHandler: setEditModeHandler,
      params: [{ id, body }]
    },
    {
      name: 'delete',
      icoName: IconName.DELETE,
      handler: null,
      pressHandler: handleCommentDelete,
      params: id
    }
  ];
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <div>
          {renderCommentBody()}
          <div className={styles.row}>
            {labels.map(el =>
              renderIconLabel({
                onPress: el.pressHandler,
                icoName: el.icoName,
                handler: el.handler,
                params: el.params
              })
            )}
          </div>
        </div>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  editComment: commentType.func
};

export default Comment;
